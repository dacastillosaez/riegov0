#ifndef _MANAGER_H
#define _MANAGER_H

#include "electrovalvula.h"
#include "memory_process.h"
#include "voltage_monitor.h"

/*
V0 - Temperatura (S)
V1 - Humedad (S)
V2 - Fecha Hora (E)
V3 - Estado del Riego (S)
V4 - Tiempo de Riego (E)
V5 - Humedad Suelo (N/A)
V6 - Indicador Caudal (N/A)
V7 - Indicador Lluvia (N/A)
V8 - Sector1 (E)
V9 - Sector2 (E) (N/A)
V10 - Sector3 (E) (N/A)
V11 - Sector4 (E)  (N/A)
V12 - Balsa (E)
V13 - Pueblo (E) (N/A)
V14 - Ciclos Riego (E)
V15 - SystemRestart (E)
V16 - Carga Bateria(%) (S)


*/
#define SeialAT Serial1;

extern Server_Variables server_data;
   /*struct {
      uint8 pueblo : 1;
      uint8 Balsa : 1;
      uint8 Sector1 : 1;
      uint8 Sector2 : 1;
      uint8 Sector3 : 1;
      uint8 Sector4 : 1;
    }Sectores;*/
enum StatusRiego
{
  PREPARADO= 0U,
  REGANDO = 1U,
  CONDICIONES_ERRONEAS,
  RIEGO_TERMINADO
};

enum TypeRiego
{
  OFF = 0U,
  RIEGO_BALSA_SECTORES = 1U,
  RIEGO_PUEBLO_SECTOR1,
  RIEGO_PUEBLO_SECTOR2,
  RIEGO_PUEBLO_SECTOR3,
  RIEGO_PUEBLO_SECTOR4
};

enum StatusSector
{
  DESACTIVADO = 0U,
  ACTIVADO
};

enum ModeConexion
{
  MANUAL = 0U,
  AUTOMATICO
};


class Manager {   
  public:             // Access specifier

public:
    //! @brief Obtiene una instancia al objeto singleton
    static Manager * instance();
 
  Manager();

  void init_configuration();

  void start();

  static void sendData();

  static void readData();

  static void riegoBalsa(Electrovalvula* _sector1, Electrovalvula* _sector2, Electrovalvula* _sector3, Electrovalvula* _sector4, Electrovalvula* _balsa, Electrovalvula* _pueblo);

  static void riegoPueblo(Electrovalvula* _sector1, Electrovalvula* _sector2, Electrovalvula* _sector3, Electrovalvula* _sector4, Electrovalvula* _balsa, Electrovalvula* _pueblo);

  static void riegoTerminado();

  static void apagarValvulas(Electrovalvula* _sector1, Electrovalvula* _sector2, Electrovalvula* _sector3, Electrovalvula* _sector4, Electrovalvula* _balsa, Electrovalvula* _pueblo);

  static void updateDisplay(uint8 estado);

  static bool checkConditionsToReady();

  static bool areIncorrectConditions();

  static void restartGSM();

  static void systemRestart();

  static void ISR_Blink();

  static void calculateBateria();

  private: private:
    static Manager * m_instance; //!< Instancia del objeto singleton


};


#endif
