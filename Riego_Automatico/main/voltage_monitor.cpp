#include "voltage_monitor.h"
#include <LowPower.h>
using namespace std;


VoltageMonitor::VoltageMonitor()
{
      adc_value = 0u;
      arduino_voltage = 0u;
      voltage_battery = 0u;
}

void VoltageMonitor::readBaterryVoltage()
{
}

float VoltageMonitor::getBatteryVoltage()
{
      adc_value = analogRead(ADC_BATTERY_PIN);   // Leemos el valor del ADC
      arduino_voltage = (adc_value*5)/1024;
      voltage_battery = (arduino_voltage/0.3125);
      return voltage_battery;
}

void setArduinoToSleepMode()
 {
       // Incluir la libreria de LowPower
    LowPower.powerDown(SLEEP_60S, ADC_OFF, BOD_OFF); 

     Serial.println("Hola");
     delay(1000);
     Serial.println("Adios");
   
 }

void setArduinoToNormalMode()
{

    
}

