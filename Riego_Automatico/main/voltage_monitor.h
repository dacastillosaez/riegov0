#ifndef _VOLTAGEMONITOR_H
#define _VOLTAGEMONITOR_H

#include "Types.h"
#include <Arduino.h>
/* Las resistencias del divisor de tensión son de 2.2K y 1K. El ADC del Arduino puede leer
rango de tensiones de [0-5] V, por tanto, el voltage que el Arduino lee se calcula:
V = Vbateria*0,3125, siendo 0,3125 el cociente entre R2/(R1+R2)
La resolucion del ADC es de 1024, por tanto, 1024 corresponde a 5V*/

#define MIN_VOLTAGE 12.1
#define MAX_VOLTAGE 13

class VoltageMonitor 
{   
  private:

  uint16 adc_value; // Valor leido por el ADC del Arduino
  float arduino_voltage; // Valor en Voltios transformado del ADC
  float voltage_battery; // Voltage de la bateria

  public:             // Access specifier

  VoltageMonitor();
  
    void readBaterryVoltage(); 
    float getBatteryVoltage();
    void setArduinoToSleepMode();
    void setArduinoToNormalMode();

    
};


 

#endif