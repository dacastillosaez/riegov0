#include "manager.h"
//#include <TinyGsmClient.h>
#include <BlynkSimpleTinyGSM.h>
#include <TimerOne.h>
using namespace std;

//-----------------------------------------------------------------------
// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = BLYNK_AUTH_TOKEN;
//-----------------------------------------------------------------------
// Your GPRS credentials
// Leave empty, if missing user or pass
char apn[]  = "Digi";
char user[] = "";
char pass[] = "";

DHT sensor( DHT11_PIN, DHTTYPE);
TinyGsm modem(SerialAT);
BlynkTimer timer;
Server_Variables server_data;
VoltageMonitor voltage_monitor;
MemoryProcess memory_manager;



Manager::Manager()
{
   Serial.println("----------CREANDO MANAGER-----");

}

Manager *Manager::instance()
{
  if(m_instance == nullptr)
    {
          m_instance = new Manager();
    }
  return m_instance;
}
static void Manager::sendData()
{

  Serial.println("TIMER SENDATA");

  SerialAT.println("AT+CCLK?"); // Comando para solicitar la hora
  server_data.fechaHora = SerialAT.readString();
  server_data.fechaHora.remove(1,9); //Eliminamos los caracteres del 1-9 y del 18-10
  server_data.fechaHora.remove(15,10); // La fecha lo conforma los caracteres de [10-17]



  //TODO->Reportarlo solo cuando se riega
  server_data.humedad = sensor.readHumidity(); 
  server_data.temperatura = sensor.readTemperature();
  Blynk.virtualWrite(V0, server_data.temperatura);
  Blynk.virtualWrite(V1, server_data.humedad);
  Blynk.virtualWrite(V2, server_data.fechaHora);

  Blynk.virtualWrite(V8, server_data.Sector1);
  Blynk.virtualWrite(V9, server_data.Sector2);
  Blynk.virtualWrite(V10, server_data.Sector3);
  Blynk.virtualWrite(V11, server_data.Sector4);
  Blynk.virtualWrite(V12, server_data.Balsa);
  Blynk.virtualWrite(V13, server_data.pueblo);
  Blynk.virtualWrite(V14, server_data.current_cycles_riego);
  Blynk.virtualWrite(V16, server_data.porcentaje_bateria);
  Blynk.virtualWrite(V4, server_data.tiempo_riego);


}

static void Manager::readData()
{
  Serial.println("TIMER READDATA");
  static uint8 sector_flags;
    if( ((server_data.pueblo == ACTIVADO) || (server_data.Balsa == ACTIVADO)) && (server_data.pueblo != server_data.Balsa)) // Es posible regar
      {
        sector_flags = server_data.Sector1 | server_data.Sector2 | server_data.Sector3 | server_data.Sector4; // TODO->CAMBIAR

        if((server_data.pueblo == ACTIVADO)  ) //Podemos regar solo un sector
        {
          if(server_data.tiempo_riego > 0U)
          {
            if(sector_flags & 0x01U) // Es posible regar con el pueblo un sector
            {
              // Riego Pueblo
              server_data.isPossibleToRegar = TRUE;
              if(server_data.isRegando == TRUE)
              {
                Manager::updateDisplay(REGANDO);
              }
              else
              {
                Manager::updateDisplay(PREPARADO);
              }
            }
            else
            {
              Manager::updateDisplay(CONDICIONES_ERRONEAS);
            }
          }
          else
          {
              server_data.estado_actual = CONDICIONES_ERRONEAS;
          }
        }
        else // Balsa esta activa y podemos regar hasta los 4 sectores
        {
          if(server_data.tiempo_riego > 0U)
          {
            // Riego Balsa
            Serial.println("Balsa Activa y podemos regar");
            server_data.isPossibleToRegar = TRUE;
            server_data.tipo_riego = RIEGO_BALSA_SECTORES;
            server_data.estado_actual = REGANDO;
            Manager::updateDisplay(REGANDO);
          }
          else
          {
            server_data.estado_actual = CONDICIONES_ERRONEAS;
            Manager::updateDisplay(CONDICIONES_ERRONEAS);
          }   
        }
      }
      else // O los 2 estan deseleccionados o los 2 estan seleccionados
      {
          //Chequear que no esten todos a 0
        if( (server_data.pueblo == ACTIVADO) && (server_data.Balsa == ACTIVADO) )
        {
          server_data.estado_actual = CONDICIONES_ERRONEAS;
          Manager::updateDisplay(CONDICIONES_ERRONEAS); 
        }
        else // Los 2 estan desactivados
        {
          server_data.estado_actual = PREPARADO;
          Manager::updateDisplay(PREPARADO); 
        }
      }
  

}

void Manager::updateDisplay(uint8 estado)
{
  if(estado == CONDICIONES_ERRONEAS)
  {
    server_data.estado_actual = CONDICIONES_ERRONEAS;
    server_data.displayRiego = "CONDICIONES_ERRONEAS";
    Blynk.virtualWrite(V3, server_data.displayRiego); 
  } 
  else if (estado == PREPARADO )
  {
    server_data.estado_actual = PREPARADO;
    server_data.displayRiego = "PREPARADO";
    Blynk.virtualWrite(V3, server_data.displayRiego); 
  }
  else if (estado == REGANDO)
  {
    server_data.estado_actual = REGANDO;
    server_data.displayRiego = "REGANDO";
    Blynk.virtualWrite(V3, server_data.displayRiego); 
    Blynk.virtualWrite(V14, server_data.current_cycles_riego); 
  }
  else if( estado == RIEGO_TERMINADO)
  {
    server_data.estado_actual = RIEGO_TERMINADO;
    server_data.displayRiego = "RIEGO_TERMINADO";
    Blynk.virtualWrite(V3, server_data.displayRiego); 
  }
  else // Este corresponde a la primera vez que arranca para resetar el display
  {
    Blynk.virtualWrite(V4, 0U); 
    Blynk.virtualWrite(V7, 0U); 
    Blynk.virtualWrite(V8, 0U); 
    Blynk.virtualWrite(V9, 0U); 
    Blynk.virtualWrite(V10, 0U); 
    Blynk.virtualWrite(V11, 0U); 
    Blynk.virtualWrite(V12, 0U); 
    Blynk.virtualWrite(V13, 0U); 
    Blynk.virtualWrite(V14, 0U); 
    Blynk.virtualWrite(V15, 0U); 

  }
}
static void interruptnmanualButonBalsa()
{
  server_data.modo_conexion = MANUAL;
  server_data.Balsa = TRUE;
}

static void interruptnmanualButonPueblo()
{
  server_data.modo_conexion = MANUAL;
  server_data.pueblo = TRUE;
}

static void interruptmanualButonSector1()
{
  server_data.Sector1 = TRUE;
}

static void interruptmanualButonSector2()
{
  server_data.Sector2 = TRUE;
}
static void interruptmanualButonSector3()
{
  server_data.Sector3 = TRUE;
}

static void interruptmanualButonSector4()
{
  server_data.Sector4 = TRUE;
}

void Manager::init_configuration()
{
  server_data.modo_conexion = AUTOMATICO; // Por defecto se inicializa el sistema en modo automtico (GSM)

  Timer1.initialize(5000000);         // Dispara cada 5s
  Timer1.attachInterrupt(ISR_Blink); // Activa la interrupcion y la asocia a ISR_Blink

  pinMode(MANUAL_PIN_BALSA, INPUT_PULLUP);
  pinMode(MANUAL_PIN_PUEBLO, INPUT_PULLUP);
  pinMode(MANUAL_PIN_SECTOR1, INPUT_PULLUP);
  pinMode(MANUAL_PIN_SECTOR2, INPUT_PULLUP);
  pinMode(MANUAL_PIN_SECTOR3, INPUT_PULLUP);
  pinMode(MANUAL_PIN_SECTOR4, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(MANUAL_PIN_BALSA), interruptnmanualButonBalsa , FALLING);//configuramos la interrupción externa
  attachInterrupt(digitalPinToInterrupt(MANUAL_PIN_PUEBLO), interruptnmanualButonPueblo , FALLING);//configuramos la interrupción externa
  attachInterrupt(digitalPinToInterrupt(MANUAL_PIN_SECTOR1), interruptmanualButonSector1 , FALLING);//configuramos la interrupción externa
  attachInterrupt(digitalPinToInterrupt(MANUAL_PIN_SECTOR2), interruptmanualButonSector2 , FALLING);//configuramos la interrupción externa
  attachInterrupt(digitalPinToInterrupt(MANUAL_PIN_SECTOR3), interruptmanualButonSector3 , FALLING);//configuramos la interrupción externa
  attachInterrupt(digitalPinToInterrupt(MANUAL_PIN_SECTOR4), interruptmanualButonSector4 , FALLING);//configuramos la interrupción externa


  //Manager::restartGSM();
  pinMode(SOFTWARE_ENABLE_GSM, OUTPUT);//ledpinpin :pin 9 par encendido del gsm por software
  pinMode(POWER_ENABLE_GSM, OUTPUT);//rele del sim900

  Serial.begin(9600);
  
  //memory_manager.writeData(&server_data, DEFECTO);
  int tamano_string = sizeof(server_data.fechaHora);
  int tamano_float = sizeof(server_data.temperatura);
  int tamano_uint8 = sizeof(server_data.Sector1);
  int tamano_bool = sizeof(server_data.isPossibleToRegar);
  /*Serial.println("Tamaño del string "+(String)tamano_string);
  Serial.println("Tamaño del string "+(String)tamano_float);
  Serial.println("Tamaño del string "+(String)tamano_uint8);
  Serial.println("Tamaño del string "+(String)tamano_bool);*/
  SerialAT.begin(115200);  // Set GSM module baud rate
  delay(3000);
  sensor.begin();

  /*digitalWrite(POWER_ENABLE_GSM , HIGH);
  //encendido del gsm por software con un pulso en ledpin:pin 9
  digitalWrite(SOFTWARE_ENABLE_GSM , HIGH);
  delay(1000);
  digitalWrite(SOFTWARE_ENABLE_GSM , LOW);
  delay(3000);   */
  Manager::restartGSM();
 


 // Desbloqueada la tarjeta sim
  
  //conecta con el servidor en la nube blynk
  Blynk.begin(auth, modem, apn, user, pass);

  //-----------------------------------------------------------
  // sendData es la funcion que se ejecuta cada INTERVAL
  timer.setInterval(INTERVAL_SENDDATA, Manager::sendData);
  timer.setInterval(INTERVAL_READDATA, Manager::readData);
  server_data.tipo_riego = OFF;

}


void Manager::start()
{
 timer.run();//un sólo objeto timer con dferentes intervalos y funciones cada intervalo
 Blynk.run(TRUE);

}


static void Manager::riegoBalsa(Electrovalvula* _sector1, Electrovalvula* _sector2, Electrovalvula* _sector3, Electrovalvula* _sector4, Electrovalvula* _balsa, Electrovalvula* _pueblo)
{

  //Primera vez
   if((server_data.tiempo_riego > 0U) && ( server_data.current_cycles_riego <= server_data.tiempo_riego ))
  {
    if(server_data.isRegando == FALSE) // Si no estamos regand, activamos las electrovalvulas la primera vez
    {
      server_data.isRegando = TRUE;
      _balsa->encender_electrovalvula();
      _sector1->encender_electrovalvula();
      _sector2->encender_electrovalvula();
      _sector3->encender_electrovalvula();
      _sector4->encender_electrovalvula();

    }
    server_data.current_cycles_riego++;
  }

  // Entra la ultima vez
  if((server_data.current_cycles_riego == (server_data.tiempo_riego + 1U)) && ( server_data.isRegando == TRUE))
  {
    Manager::apagarValvulas(_sector1,_sector2,_sector3,_sector4,_balsa,_pueblo);
    server_data.estado_actual = RIEGO_TERMINADO;
    
    server_data.isRegando = FALSE;
    server_data.isPossibleToRegar = FALSE;
    server_data.current_cycles_riego = 0U;
    server_data.tiempo_riego = 0U;
    server_data.Sector1 = 0U; 
    server_data.Balsa = 0U;
  }

}


static void Manager::riegoPueblo(Electrovalvula* _sector1, Electrovalvula* _sector2, Electrovalvula* _sector3, Electrovalvula* _sector4, Electrovalvula* _balsa, Electrovalvula* _pueblo )
{

  //Primera vez
   if((server_data.tiempo_riego > 0U) && ( server_data.current_cycles_riego < server_data.tiempo_riego ))
  {
    if(server_data.isRegando == FALSE) // Si no estamos regand, activamos las electrovalvulas la primera vez
    {
      server_data.isRegando = TRUE;
      switch (server_data.tipo_riego)
      {
      case RIEGO_PUEBLO_SECTOR1:
      {
        _pueblo->encender_electrovalvula();
        _sector1->encender_electrovalvula();
      }
        break;
      case RIEGO_PUEBLO_SECTOR2:
      {
        _pueblo->encender_electrovalvula();
        _sector2->encender_electrovalvula();
      }
        break;

      case RIEGO_PUEBLO_SECTOR3:
      {
        _pueblo->encender_electrovalvula();
        _sector3->encender_electrovalvula();
      }
        break;

      case RIEGO_PUEBLO_SECTOR4:
      {
        _pueblo->encender_electrovalvula();
        _sector4->encender_electrovalvula();
      }
        break;
      
      default:
      {
       Manager::apagarValvulas(_sector1,_sector2,_sector3,_sector4,_balsa,_pueblo);
      }
        break;
      }
    }
    server_data.current_cycles_riego++;
  }

  // Entra la ultima vez
  if((server_data.current_cycles_riego == server_data.tiempo_riego) && ( server_data.isRegando == TRUE))
  {
    Manager::apagarValvulas(_sector1,_sector2,_sector3,_sector4,_balsa,_pueblo);
    server_data.estado_actual = RIEGO_TERMINADO;
    
    server_data.isRegando = FALSE;
    server_data.isPossibleToRegar = FALSE;
    server_data.current_cycles_riego = 0U;
    server_data.tiempo_riego = 0U;
    server_data.Sector1 = 0U; 
    server_data.Balsa = 0U;
  }
}

static void Manager::apagarValvulas(Electrovalvula* _sector1, Electrovalvula* _sector2, Electrovalvula* _sector3, Electrovalvula* _sector4, Electrovalvula* _balsa, Electrovalvula* _pueblo)
{
  _balsa->apagar_electrovalvula();
  _pueblo->apagar_electrovalvula();
  _sector1->apagar_electrovalvula();
  _sector2->apagar_electrovalvula();
  _sector3->apagar_electrovalvula();
  _sector4->apagar_electrovalvula();
}

BLYNK_WRITE(V4)
{ 
  server_data.tiempo_riego = param.asInt();
}

BLYNK_WRITE(V5)
{ 
  server_data.humedad_suelo = param.asInt();
}

BLYNK_WRITE(V6)
{ 
  server_data.indicador_caudal = param.asInt();
}

BLYNK_WRITE(V7)
{ 
  server_data.indicador_lluvia = param.asInt();
}

BLYNK_WRITE(V8)
{

  server_data.Sector1 = param.asInt();
  if(server_data.Sector1 == DESACTIVADO)
  {
    //TODO->Apagar electrovalvulas
    //Sector1.apagar_electrovalvula();
  }

}

BLYNK_WRITE(V9)
{
  server_data.Sector2 = param.asInt();
  if(server_data.Sector2 == DESACTIVADO)
  {
    //Sector2.apagar_electrovalvula();
  }
}
BLYNK_WRITE(V10)
{
  server_data.Sector3 = param.asInt();
  if(server_data.Sector3 == DESACTIVADO)
  {
    //Sector3.apagar_electrovalvula();
  }
}

BLYNK_WRITE(V11)
{
 server_data.Sector4 = param.asInt();
 if(server_data.Sector4 == DESACTIVADO)
 {
    //Sector4.apagar_electrovalvula();
 }
}

BLYNK_WRITE(V12) // Lee de la interfaz grafica
{
  server_data.Balsa = param.asInt();  
  if(server_data.Balsa == DESACTIVADO)
  {
      //Balsa.apagar_electrovalvula();
  }
}

BLYNK_WRITE(V13)
{
  server_data.pueblo = param.asInt(); 
  if(server_data.pueblo == DESACTIVADO)
  {
    //Pueblo.apagar_electrovalvula();
  }
}

BLYNK_WRITE(V15)
{
  Manager::systemRestart();
}

/*
BLYNK_CONNECTED()
{
  Blynk.virtualWrite(V4, server_data.humedad); 
  Blynk.virtualWrite(V4, server_data.temperatura); 
}*/

void Manager::restartGSM()
{
   //encendido del gsm por software con un pulso en ledpin:pin 9
  digitalWrite(SOFTWARE_ENABLE_GSM , HIGH);
  delay(1000);
  digitalWrite(SOFTWARE_ENABLE_GSM , LOW);
  delay(3000); 
  digitalWrite(SOFTWARE_ENABLE_GSM , HIGH );
  delay(3000); 
}

bool Manager::checkConditionsToReady()
{

}

bool Manager::areIncorrectConditions()
{

}

 void Manager::systemRestart()
 {
 // TODO-> Aqui debemos apagar las electrovalvulas y toda la parte mecanica
  asm("jmp 0x0000");
 }


static void Manager::ISR_Blink()         // interrupción por igualdade de comparación en TIMER1
{
  static Electrovalvula Sector1(1u,PULSE_EN_ELECTR_A,POSITIVE_WIRE_ELECTR_A,NEGATIVE_WIRE_ELECTR_A,PIN_RELE_ELECTR_A);
  static Electrovalvula Sector2(2u,PULSE_EN_ELECTR_B,POSITIVE_WIRE_ELECTR_B,NEGATIVE_WIRE_ELECTR_B,PIN_RELE_ELECTR_B);
  static Electrovalvula Sector3(3u,PULSE_EN_ELECTR_C,POSITIVE_WIRE_ELECTR_C,NEGATIVE_WIRE_ELECTR_C,PIN_RELE_ELECTR_C);
  static Electrovalvula Sector4(4u,PULSE_EN_ELECTR_D,POSITIVE_WIRE_ELECTR_D,NEGATIVE_WIRE_ELECTR_D,PIN_RELE_ELECTR_D);
  static Electrovalvula Pueblo(5u,PULSE_EN_ELECTR_PUEBLO,POSITIVE_WIRE_ELECTR_PUEBLO,NEGATIVE_WIRE_ELECTR_PUEBLO,PIN_RELE_ELECTR_PUEBLO);
  static Electrovalvula Balsa(6u,PULSE_EN_ELECTR_BALSA,POSITIVE_WIRE_ELECTR_BALSA,NEGATIVE_WIRE_ELECTR_BALSA,PIN_RELE_ELECTR_BALSA);
  //memory_manager.readData(&server_data, DATOS_GUARDADOS);
  switch (server_data.estado_actual)
  {
    case PREPARADO:
    {
      Serial.println("ESTADO PREPARADO");
      if(server_data.isPossibleToRegar == TRUE)
      {
        server_data.estado_previo = server_data.estado_actual;
        server_data.estado_actual = REGANDO;
      }
      else if( Manager::areIncorrectConditions() == TRUE)
      {
        server_data.estado_previo = server_data.estado_actual;
        server_data.estado_actual = CONDICIONES_ERRONEAS;
      }
      else
      {
        server_data.estado_actual = PREPARADO;
      }
    }
      break;

      case REGANDO:
    {
        Serial.println("ESTADO REGANDO");
        // Chequear condiciones de si esta regando y se cumplen condiciones erroneas de Botones, parar el riego
        
        if(server_data.pueblo == TRUE)
        {
          Manager::riegoPueblo(&Sector1,&Sector2, &Sector3, &Sector4, &Balsa, &Pueblo);
        }
        else if(server_data.Balsa == TRUE)
        {
          Manager::riegoBalsa(&Sector1,&Sector2, &Sector3, &Sector4, &Balsa, &Pueblo);
        }
        else
        {
          //NO HACEMOIS NADA POR AHORA
        }
    }
      break;

    case CONDICIONES_ERRONEAS:
    {
      Serial.println("CONDICIONES_ERRONEAS REGANDO");
      if(Manager::areIncorrectConditions() == TRUE) // Si se cumplen las condiciones para transicionar a PREPARADO
      {
        server_data.estado_previo = server_data.estado_actual;
        server_data.estado_actual = CONDICIONES_ERRONEAS;
      }
      else
      {
        server_data.estado_previo = server_data.estado_actual;
        server_data.estado_actual = PREPARADO;
      }
    }
      break;

    case RIEGO_TERMINADO:
    {
      if(Manager::checkConditionsToReady() == TRUE) // Si se cumplen las condiciones para transicionar a PREPARADO
      {
        server_data.estado_previo = server_data.estado_actual;
        server_data.estado_actual = PREPARADO;
      }
      else
      {
        server_data.estado_actual = RIEGO_TERMINADO;
      }    
    }
      break;


    default:
      break;
  }
  //teBateria();

  Serial.println("Timer");
  Serial.println("Ciclos de Riego Actuales: "+ (String)server_data.current_cycles_riego);
  Serial.println("Tiempo de Riego: "+ (String)server_data.tiempo_riego);
  Serial.println("Esta Regando?: "+ (String)server_data.isRegando);
  Serial.println("Estado Sector1: "+ (String)server_data.Sector1);
  Serial.println("Estado Sector2: "+ (String)server_data.Sector2);
  Serial.println("Estado Sector3: "+ (String)server_data.Sector3);
  Serial.println("Estado Sector4: "+ (String)server_data.Sector4);
  Serial.println("Estado Balsa: "+ (String)server_data.Balsa);
  Serial.println("Estado Pueblo: "+ (String)server_data.pueblo);
  Serial.println("Es Posible Regar: "+ (String)server_data.isPossibleToRegar);
  Serial.println("Nivel de Bateria: "+ (String)server_data.porcentaje_bateria);
  Serial.println("Estado Actual: "+ (String)server_data.estado_actual);
  
  Serial.println("----------------------------------------");
  Serial.println("");
}

void Manager::calculateBateria()
{
  static float result = 0U;
  result = voltage_monitor.getBatteryVoltage();
  if(result <= MIN_VOLTAGE)
  {
    //Cerramos todas las electrovalvulas y apagamos el Arduino
  }
    Serial.println("VoltageBateria: "+(String)result);
  //Calculamos el porcentaje de Bateria
  server_data.porcentaje_bateria = (result*100)/MAX_VOLTAGE;

}