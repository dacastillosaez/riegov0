#include "memory_process.h"

using namespace std;


MemoryProcess::MemoryProcess()
{
     
}

void MemoryProcess::writeData(Server_Variables* data, boolean dir)
{
    if(dir == DEFECTO)
    {
        EEPROM.put( DIR_STRUCT_DEFECTO, *data );
    }
    else if(dir == DATOS_GUARDADOS)
    {
        EEPROM.put( DIR_STRUCT_GUARDADA, *data );
    }
    else
    {
        //No hacer nada
    }
}

void MemoryProcess::readData(Server_Variables* data, boolean dir)
{
    if(dir == DEFECTO)
    {
        EEPROM.get( DIR_STRUCT_DEFECTO, *data ); // Cogemos los valores desde la direccion 0x0
    }
    else if(dir == DATOS_GUARDADOS)
    {
        EEPROM.get( DIR_STRUCT_GUARDADA, *data ); // Cogemos los datos guardados desde la direccion 0x1F4
    }
    else
    {
        //No hacer nada
    }
    Serial.println("Leyendo valor de memoria Balsa: " +(String)data->Balsa);
    Serial.println("Leyendo valor de memoria Pueblo: " +(String)data->pueblo);
    Serial.println("Leyendo valor de memoria Sector1: " +(String)data->Sector1);
    Serial.println("Leyendo valor de memoria Sector2: " +(String)data->Sector2);
}


