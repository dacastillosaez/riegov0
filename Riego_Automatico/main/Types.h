#ifndef _TYPES_H
#define _TYPES_H

typedef unsigned char uint8;
typedef unsigned short int uint16;
typedef unsigned int uint32;
typedef unsigned long int uint64;

#define ADC_BATTERY_PIN 0U
#define FALSE 0U
#define TRUE 1U

#define DIR_STRUCT_GUARDADA 0x1F4 // Empieza en 500 bytes
#define DIR_STRUCT_DEFECTO 0x0 // Empieza en la direccion 0x0
#include <avr/wdt.h>
#include <String.h>
#include <SoftwareSerial.h> 


enum TipoEstructura
{
  DEFECTO = 0U,
  DATOS_GUARDADOS = 1U,
};

typedef struct
{
    float temperatura; 
    float humedad;
    float humedad_suelo;
    uint8 porcentaje_bateria;
    uint8 indicador_caudal;
    uint8 indicador_lluvia;
    uint8 tiempo_riego;
    uint8 estado_actual;
    uint8 estado_previo;
    uint8 pueblo;
    uint8 cicloRiego;
    uint8 Balsa;
    uint8 Sector1;
    uint8 Sector2;
    uint8 Sector3;
    uint8 Sector4;
    uint8 isRegando;
    uint8 current_cycles_riego;
    String fechaHora;
    String displayRiego;
    bool isPossibleToRegar;
    bool isConnected;
    uint8 tipo_riego; // Indica que tipo de riego queremos hacer. Ej: balsa-Sector1, Pueblo-Sector2... etc
    bool modo_conexion; // Indica que tipo conexion: Manual/Automatico
} Server_Variables;

typedef struct
{
    float temperatura; 
    float humedad;
    String fechaHora;

}SavedData;

 
#endif