#include "centralita_sms.h"
#include"manager.h"
#include<Arduino.h>

using namespace std;
CentralitaSms centralita;

CentralitaSms::CentralitaSms( )
{mensaje_recibido="";
mensaje_enviar="";
}

 void CentralitaSms::recibir_mensaje_sms()
{ SerialAT.print("AT+CMGF=1\r");//prepara el puerto serie en modo texto
  SerialAT.print("AT+CNMI=2,2,0,0,0\r");//muestra el sms por puerto serie
  if(SerialAT.available()) mensaje_recibido=SerialAT.readString();
}

void CentralitaSms::procesar_mensaje_sms()
{long orden_riego;

orden_riego=mensaje_recibido.toInt();
    
}


void CentralitaSms::mandar_mensaje_sms()

{SerialAT.print("AT+CMGF=1\r");//prepara el puerto serie en modo texto
 delay(200);
 SerialAT.println("AT+CMGS=\"XXXXXXXXXX\"");//reemplazar XXXXXX por el número a enviar el mensaje
 delay(200);
 SerialAT.println(mensaje_enviar);// enviamos el mensaje
 delay(200);
 SerialAT.println(char(26));// fin de sms
 delay(200);
 SerialAT.println();
}
