#ifndef _ELECTROVALVULE_H
#define _ELECTROVALVULE_H

#include <DHT.h>
#include "Types.h"


//-----------------------------------------------------------------------
#define BLYNK_PRINT Serial
//Datos suministrados por Blynk para el servidor en la nube
#define BLYNK_TEMPLATE_ID "TMPLe2eKY_RO"
#define BLYNK_DEVICE_NAME "RiegoRamon"
#define BLYNK_AUTH_TOKEN "zqOrzdCYl-pSyNG5RQovzPuQ7-Vrse93" //Datos necesarios para crear el cliente del servidor Blynk en la nube

#define TINY_GSM_MODEM_SIM900
// Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial3

#define DHT11_PIN 4U
#define DHTTYPE DHT11
#define INTERVAL_SENDDATA 5000U // 10s (3min)
#define INTERVAL_READDATA 20000U // 3s

//---------------V A L V U L A   S E C T O R   1--------------------------------------------------------
#define PIN_RELE_ELECTR_A 11U // Activa electrovalvulas 2 a 2, sin dar pulsos
#define PULSE_EN_ELECTR_A 8U // Disparo Electrovalvula A
#define POSITIVE_WIRE_ELECTR_A 22U // Polo positivo Electrovalvula A
#define NEGATIVE_WIRE_ELECTR_A 23U // Polo negativo Electrovalvula A

//---------------V A L V U L A   S E C T O R   2--------------------------------------------------------
#define PIN_RELE_ELECTR_B 11U // Activa electrovalvulas 2 a 2, sin dar pulsos
#define PULSE_EN_ELECTR_B 13U // Disparo Electrovalvula Sector 2
#define POSITIVE_WIRE_ELECTR_B 24U // Polo positivo Electrovalvula Sector 2
#define NEGATIVE_WIRE_ELECTR_B 25U // Polo negativo Electrovalvula Sector 2

//---------------V A L V U L A   S E C T O R   3--------------------------------------------------------
#define PIN_RELE_ELECTR_C 12U // Activa electrovalvulas 2 a 2, sin dar pulsos
#define PULSE_EN_ELECTR_C 16U // Disparo Electrovalvula Sector 3
#define POSITIVE_WIRE_ELECTR_C 26U // Polo positivo Electrovalvula Sector 3
#define NEGATIVE_WIRE_ELECTR_C 27U // Polo negativo Electrovalvula Sector 3

//---------------V A L V U L A   S E C T O R   4--------------------------------------------------------
#define PIN_RELE_ELECTR_D 12U // Activa electrovalvulas 2 a 2, sin dar pulsos
#define PULSE_EN_ELECTR_D 5U // Disparo Electrovalvula Sector 4
#define POSITIVE_WIRE_ELECTR_D 28U // Polo positivo Electrovalvula Sector 4
#define NEGATIVE_WIRE_ELECTR_D 29U // Polo negativo Electrovalvula Sector 4

//---------------V A L V U L A   B A L S A --------------------------------------------------------
#define PIN_RELE_ELECTR_BALSA 10U // Activa electrovalvulas 2 a 2, sin dar pulsos
#define PULSE_EN_ELECTR_BALSA 6U // Disparo Electrovalvula Balsa
#define POSITIVE_WIRE_ELECTR_BALSA 30U // Polo positivo Electrovalvula Balsa
#define NEGATIVE_WIRE_ELECTR_BALSA 31U // Polo negativo Electrovalvula Balsa

//---------------V A L V U L A   P U E B L O --------------------------------------------------------
#define PIN_RELE_ELECTR_PUEBLO 10U // Activa electrovalvulas 2 a 2, sin dar pulsos
#define PULSE_EN_ELECTR_PUEBLO 7U // Disparo Electrovalvula PUEBLO
#define POSITIVE_WIRE_ELECTR_PUEBLO 32U // Polo positivo Electrovalvula PUEBLO
#define NEGATIVE_WIRE_ELECTR_PUEBLO 33U // Polo negativo Electrovalvula PUEBLO

//---------------B O T O N E S   M A N U A L E S --------------------------------------------------------
#define MANUAL_PIN_BALSA 2U // 
#define MANUAL_PIN_PUEBLO 3U // 
#define MANUAL_PIN_SECTOR1 18U // 
#define MANUAL_PIN_SECTOR2 19U // 
#define MANUAL_PIN_SECTOR3 20U // 
#define MANUAL_PIN_SECTOR4 21U // 

//---------------P I N E S   G S M --------------------------------------------------------
#define SOFTWARE_ENABLE_GSM  9U // Encender por softare el GSM
#define POWER_ENABLE_GSM  8U  // Pin digital que habilita le Rele de la SIM
#define ARDUINO_RESET  12U  // Pin para resetear el arduino


#define DELAY_IMPULSE_ELECTROVALVULE 60U

//-----------------------------------------------------------------------

class Electrovalvula 
{   
  public:             // Access specifier

  typedef struct
  {
  uint8 id; // Identificador de la valvula
  uint8 pulse_valule;
  uint8 positive_wire;
  uint8 negative_wire;
  uint8 rele_wire;
  uint8 current_status;
  } Valvule;


 
Valvule valvule_data;

  Electrovalvula(uint8 _id, uint8 _pulse_valvule, uint8 _positive_wire, uint8 _negative_wire, uint8 _rele_wire);
  
    void init_electrovalvula(); 
    void encender_electrovalvula(); 
    void apagar_electrovalvula();
    
};

class Riego : public Electrovalvula 
{   
  public:             // Access specifier
  uint8 type_riego;

};


 
#endif