#ifndef _MEMORYMANAGER_H
#define _MEMORYMANAGER_H

#include "Types.h"
#include <Arduino.h>
#include <EEPROM.h>
/* Las resistencias del divisor de tensión son de 2.2K y 1K. El ADC del Arduino puede leer
rango de tensiones de [0-5] V, por tanto, el voltage que el Arduino lee se calcula:
V = Vbateria*0,3125, siendo 0,3125 el cociente entre R2/(R1+R2)
La resolucion del ADC es de 1024, por tanto, 1024 corresponde a 5V*/

class MemoryProcess 
{   
  public:             // Access specifier

  MemoryProcess();
  
  void writeData(Server_Variables* data, boolean dir); 
  void readData(Server_Variables* data, boolean dir);

    
};


 

#endif