#include "electrovalvula.h"

using namespace std;



Electrovalvula::Electrovalvula(uint8 _id, uint8 _pulse_valvule, uint8 _positive_wire, uint8 _negative_wire, uint8 _rele_wire)
{
      valvule_data.id = _id;
      valvule_data.pulse_valule = _pulse_valvule;
      valvule_data.positive_wire = _positive_wire;
      valvule_data.negative_wire = _negative_wire;
      valvule_data.rele_wire = _rele_wire;
      valvule_data.current_status = 0U;

      //-----------------------------------------------------------
      pinMode(valvule_data.positive_wire, OUTPUT);
      pinMode(valvule_data.negative_wire , OUTPUT);
      pinMode(valvule_data.rele_wire, OUTPUT);
      //enciende el rele electrovalvula 11 
      digitalWrite(valvule_data.rele_wire,HIGH);
      //apaga la electrovalvula Invirtiendo la polaridad
      digitalWrite(valvule_data.positive_wire, LOW);
      digitalWrite(valvule_data.negative_wire, HIGH); 
     analogWrite(valvule_data.pulse_valule,255);
      digitalWrite(valvule_data.rele_wire,LOW);
      Serial.println("Creando Valvula: " + (String)valvule_data.id);

}

void Electrovalvula::init_electrovalvula()
{
      digitalWrite(PIN_RELE_ELECTR_A, HIGH); 
      digitalWrite(POSITIVE_WIRE_ELECTR_A, LOW);
      digitalWrite(NEGATIVE_WIRE_ELECTR_A, HIGH); 
      analogWrite(PULSE_EN_ELECTR_A,255);
      delay(DELAY_IMPULSE_ELECTROVALVULE);
      digitalWrite(PIN_RELE_ELECTR_A, LOW); 
}

void Electrovalvula::encender_electrovalvula()
{
      digitalWrite(valvule_data.rele_wire, HIGH); // Pin donde se activa el Rele de la Electrovalvula
      digitalWrite(valvule_data.positive_wire, HIGH);
      digitalWrite(valvule_data.negative_wire, LOW); 
      analogWrite(valvule_data.pulse_valule,255);
      delay(DELAY_IMPULSE_ELECTROVALVULE); // 60ms
      digitalWrite(valvule_data.rele_wire, HIGH);
      valvule_data.current_status = 1U; 
}

void Electrovalvula::apagar_electrovalvula()
{
      
      digitalWrite(valvule_data.rele_wire, HIGH); 
      digitalWrite(valvule_data.positive_wire, LOW);
      digitalWrite(valvule_data.negative_wire, HIGH); 
      analogWrite(valvule_data.pulse_valule,255);
      delay(DELAY_IMPULSE_ELECTROVALVULE);
      digitalWrite(valvule_data.rele_wire, LOW);
      valvule_data.current_status = 0U;  
}