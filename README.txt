Las librerias del proyecto se encuentran en Riego_Automatico/libraries.
Para que el programa Arduino las encuentre y compile:
	1. Archivo/Preferencias->Localizacion del proyecto->indicar la ruta donde tenemos clonado
el repositorio


Interrupciones para Botones modo manual:

https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/

const byte interruptPin = 2;
volatile byte state = LOW;

void setup() {
      Serial.begin(9600);

  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, CHANGE);
}

void loop() {
}

void blink() {
        Serial.println("Hola");

}